/*
 * Copyright 2022-2023 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

// Application headers.
#include "DevToolsWebEngineView.h"

// Qt toolkit headers.
#include <QWebEngineProfile>

// Construct the class.
DevToolsWebEngineView::DevToolsWebEngineView(QWidget *parentWidgetPointer) : QWebEngineView(parentWidgetPointer)
{
    // Create an off-the-record profile (the default when no profile name is specified).
    QWebEngineProfile *webEngineProfilePointer = new QWebEngineProfile(QLatin1String(""));

    // Create a WebEngine page.
    QWebEnginePage *webEnginePagePointer = new QWebEnginePage(webEngineProfilePointer);

    // Set the WebEngine page.
    setPage(webEnginePagePointer);
}
