/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2025 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEMPORARY_WEBENGINE_VIEW_H
#define TEMPORARY_WEBENGINE_VIEW_H

// Application headers.
#include "PrivacyWebEngineView.h"

// Qt framework headers.
#include <QWebEngineView>

class TemporaryWebEngineView : public QWebEngineView
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The standard constructor.
    explicit TemporaryWebEngineView(PrivacyWebEngineView *mainPrivacyWebEngineViewPointer);

private Q_SLOTS:
    // The private slots.
    void stopLoadingUrl();
};
#endif
