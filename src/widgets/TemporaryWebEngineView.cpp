/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2025 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "PrivacyWebEnginePage.h"
#include "TemporaryWebEngineView.h"

// Construct the class.
TemporaryWebEngineView::TemporaryWebEngineView(PrivacyWebEngineView *mainPrivacyWebEngineViewPointer) : QWebEngineView()
{
    // Create a WebEngine page using the same profile as the main privacy WebEngine, which is required in Qt 6.
    // Sharing a profile somehow causes the URL to load in the main privacy WebEngine view.
    PrivacyWebEnginePage *privacyWebEnginePagePointer = new PrivacyWebEnginePage(mainPrivacyWebEngineViewPointer->page()->profile());

    // Set the WebEngine page.
    setPage(privacyWebEnginePagePointer);

    // Connect the load started signal.
    connect(this, SIGNAL(urlChanged(const QUrl&)), this, SLOT(stopLoadingUrl()));
}

void TemporaryWebEngineView::stopLoadingUrl()
{
    // Stop loading the URL.
    stop();

    // Load the URL in the main privacy WebEngine view.
    // For some reason, the URL already gets loaded there, possibly because they share a profile.
    //mainPrivacyWebEngineViewPointer->load(url)

    // Destroy the temporary WebEngine view.
    destroy();
}
