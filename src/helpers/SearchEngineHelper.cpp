/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022, 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "SearchEngineHelper.h"

// Construct the class.
SearchEngineHelper::SearchEngineHelper() {}

QString SearchEngineHelper::getSearchUrl(const QString &searchEngineName)
{
    if (searchEngineName == QLatin1String("Mojeek"))  // Mojeek.
    {
        return QLatin1String("https://www.mojeek.com/search?q=");
    }
    else if (searchEngineName == QLatin1String("Monocles"))  // Monocles.
    {
        return QLatin1String("https://monocles.de/search?q=");
    }
    else if (searchEngineName == QLatin1String("MetaGer"))  // MetaGer.
    {
        return QLatin1String("https://metager.org/meta/meta.ger3?eingabe=");
    }
    else if (searchEngineName == QLatin1String("Google"))  // Google.
    {
        return QLatin1String("https://www.google.com/search?q=");
    }
    else if (searchEngineName == QLatin1String("Bing"))  // Bing.
    {
        return QLatin1String("https://www.bing.com/search?q=");
    }
    else if (searchEngineName == QLatin1String("Yahoo"))  // Yahoo.
    {
        return QLatin1String("https://search.yahoo.com/search?p=");
    }
    else
    {
        // Return the custom user agent.
        return searchEngineName;
    }
}
