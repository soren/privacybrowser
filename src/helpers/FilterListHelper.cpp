/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "FilterListHelper.h"
#include "structs/OverrideStruct.h"

// KDE Framework headers.
#include <KLocalizedString>

// Qt toolkit headers.
#include <QDebug>
#include <QFile>
#include <QRegularExpression>
#include <QTextStream>

// Construct the class.
FilterListHelper::FilterListHelper()
{
    // Populate the translated disposition strings.  Translated entries cannot be public static const.
    DEFAULT_STRING = i18nc("Default disposition", "Default - Allowed");
    ALLOWED_STRING = i18nc("Allowed disposition", "Allowed");
    BLOCKED_STRING = i18nc("Blocked disposition", "Blocked");

    // Populate the translated filter option disposition strings.  Translated entries cannot be public static const.
    FILTER_OPTION_NULL = QString();
    FILTER_OPTION_APPLY = i18nc("Apply filter option", "Apply");
    FILTER_OPTION_OVERRIDE = i18nc("Override filter option", "Override");

    // Populate the translated navigation type strings.  Translated entries cannot be public static const.
    NAVIGATION_TYPE_LINK = i18nc("Navigation type link", "Link");
    NAVIGATION_TYPE_TYPED = i18nc("Navigation type typed", "Typed");
    NAVIGATION_TYPE_FORM_SUBMITTED = i18nc("Navigation type form submitted", "Form Submitted");
    NAVIGATION_TYPE_BACK_FORWARD = i18nc("Navigation type back/forward", "Back/Forward");
    NAVIGATION_TYPE_RELOAD = i18nc("Navigation type reload", "Reload");
    NAVIGATION_TYPE_REDIRECT = i18nc("Navigation type redirect", "Redirect");
    NAVIGATION_TYPE_OTHER = i18nc("Navigation type other", "Other");

    // Populate the translated resource type strings.  Translated entries cannot be public static const.
    RESOURCE_TYPE_MAIN_FRAME = i18nc("Resource type main frame", "Main Frame");
    RESOURCE_TYPE_SUB_FRAME = i18nc("Resource type sub frame", "Sub Frame");
    RESOURCE_TYPE_STYLESHEET = i18nc("Resource type stylesheet", "Stylesheet");
    RESOURCE_TYPE_SCRIPT = i18nc("Resource type script", "Script");
    RESOURCE_TYPE_IMAGE = i18nc("Resource type image", "Image");
    RESOURCE_TYPE_FONT_RESOURCE = i18nc("Resource type font", "Font");
    RESOURCE_TYPE_SUB_RESOURCE = i18nc("Resource type sub resource", "Sub Resource");
    RESOURCE_TYPE_OBJECT = i18nc("Resource type object", "Object");
    RESOURCE_TYPE_MEDIA = i18nc("Resource type media", "Media");
    RESOURCE_TYPE_WORKER = i18nc("Resource type worker", "Worker");
    RESOURCE_TYPE_SHARED_WORKER = i18nc("Resource type shared worker", "Shared Worker");
    RESOURCE_TYPE_PREFETCH = i18nc("Resource type prefetch", "Prefetch");
    RESOURCE_TYPE_FAVICON = i18nc("Resource type favicon", "Favicon");
    RESOURCE_TYPE_XHR = i18nc("Resource type XML HTTP request", "XML HTTP Request");
    RESOURCE_TYPE_PING = i18nc("Resource type HTTP ping", "HTTP Ping");
    RESOURCE_TYPE_SERVICE_WORKER = i18nc("Resource type service worker", "Service Worker");
    RESOURCE_TYPE_CSP_REPORT = i18nc("Resource type content security policy report", "Content Security Policy Report");
    RESOURCE_TYPE_PLUGIN_RESOURCE = i18nc("Resource type plugin request", "Plugin Request");
    RESOURCE_TYPE_NAVIGATION_PRELOAD_MAIN_FRAME = i18nc("Resource type preload main frame", "Preload Main Frame");
    RESOURCE_TYPE_NAVIGATION_PRELOAD_SUB_FRAME = i18nc("Resource type preload sub frame", "Preload Sub Frame");
    RESOURCE_TYPE_UNKNOWN = i18nc("Resource type unknown", "Unknown");

    // Populate the translated sublist strings.  Translated entries cannot be public static const.
    MAIN_ALLOWLIST_STRING = i18nc("Main allowlist", "Main Allow List");
    INITIAL_DOMAIN_ALLOWLIST_STRING = i18nc("Initial domain allowlist", "Initial Domain Allow List");
    REGULAR_EXPRESSION_ALLOWLIST_STRING = i18nc("Regular expression allowlist", "Regular Expression Allow List");
    MAIN_BLOCKLIST_STRING = i18nc("Main blocklist", "Main Block List");
    INITIAL_DOMAIN_BLOCKLIST_STRING = i18nc("Initial domain blocklist", "Initial Domain Block List");
    REGULAR_EXPRESSION_BLOCKLIST_STRING = i18nc("Regular expression blocklist", "Regular Expression Block List");

    // Populate the filter lists.
    ultraPrivacyStructPointer = populateFilterList(QLatin1String(":/filterlists/ultraprivacy.txt"));
    ultraListStructPointer = populateFilterList(QLatin1String(":/filterlists/ultralist.txt"));
    easyPrivacyStructPointer = populateFilterList(QLatin1String(":/filterlists/easyprivacy.txt"));
    easyListStructPointer = populateFilterList(QLatin1String(":/filterlists/easylist.txt"));
    fanboyAnnoyanceStructPointer = populateFilterList(QLatin1String(":/filterlists/fanboy-annoyance.txt"));
}

bool FilterListHelper::checkFilterLists(PrivacyWebEngineView *privacyWebEngineViewPointer, QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer) const
{
    // Initiate a continue checking tracker.  If the tracker changes to false, all processing of the request will be stopped.
    bool continueChecking = true;

    // Get the URLs.
    QUrl firstPartyUrl = urlRequestInfo.firstPartyUrl();
    QUrl requestUrl = urlRequestInfo.requestUrl();

    // Get the hosts.
    requestStructPointer->firstPartyHost = firstPartyUrl.host();
    QString requestHost = requestUrl.host();

    // Determine if this is a third-party request.
    requestStructPointer->isThirdPartyRequest = (requestStructPointer->firstPartyHost != requestHost);

    // Get the request URL string.
    requestStructPointer->urlString = requestUrl.toString();

    // Create a URL string with separators.
    requestStructPointer->urlStringWithSeparators = requestStructPointer->urlString;

    // Replace the separators characters with `^`.
    requestStructPointer->urlStringWithSeparators.replace(QLatin1Char(':'), QLatin1Char('^'));
    requestStructPointer->urlStringWithSeparators.replace(QLatin1Char('/'), QLatin1Char('^'));
    requestStructPointer->urlStringWithSeparators.replace(QLatin1Char('?'), QLatin1Char('^'));
    requestStructPointer->urlStringWithSeparators.replace(QLatin1Char('='), QLatin1Char('^'));
    requestStructPointer->urlStringWithSeparators.replace(QLatin1Char('&'), QLatin1Char('^'));

    // Add a `^` to the end of the string it it doesn't already contain one.
    if (!requestStructPointer->urlStringWithSeparators.endsWith(QLatin1Char('^')))
        requestStructPointer->urlStringWithSeparators.append(QLatin1Char('^'));

    // Create truncated URL strings and initially populate it with the original URL strings.
    requestStructPointer->truncatedUrlString = requestStructPointer->urlString;
    requestStructPointer->truncatedUrlStringWithSeparators = requestStructPointer->urlStringWithSeparators;

    // Get the index of the beginning of the fully qualified domain name.
    int fqdnIndex = requestStructPointer->truncatedUrlString.indexOf(QLatin1String("://")) + 3;

    // Truncate the URL to the beginning of the fully qualified domain name.
    requestStructPointer->truncatedUrlString.remove(0, fqdnIndex);
    requestStructPointer->truncatedUrlStringWithSeparators.remove(0, fqdnIndex);

    // Create a filter list struct list.
    QList<FilterListStruct*> filterListStructList;

    // Populate the filter list struct.
    if (privacyWebEngineViewPointer->ultraPrivacyEnabled) filterListStructList.append(ultraPrivacyStructPointer);
    if (privacyWebEngineViewPointer->ultraListEnabled) filterListStructList.append(ultraListStructPointer);
    if (privacyWebEngineViewPointer->easyPrivacyEnabled) filterListStructList.append(easyPrivacyStructPointer);
    if (privacyWebEngineViewPointer->easyListEnabled) filterListStructList.append(easyListStructPointer);
    if (privacyWebEngineViewPointer->fanboysAnnoyanceListEnabled) filterListStructList.append(fanboyAnnoyanceStructPointer);

    // Check each filter list.
    for (FilterListStruct *filterListStructPointer : filterListStructList)
    {
        // Check the list unless a match has been found by a previous list.
        if (continueChecking)
            continueChecking = checkIndividualList(urlRequestInfo, requestStructPointer, filterListStructPointer);
    }

    // Return the continue checking status.
    return continueChecking;
}

bool FilterListHelper::checkIndividualList(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, FilterListStruct *filterListStructPointer) const
{
    // Initiate a continue checking tracker.  If the tracker changes to false, all further checking of the request will be bypasses.
    bool continueChecking = true;

    // Get the main allow list iterators.
    auto mainAllowListEntry = filterListStructPointer->mainAllowListPointer->begin();
    auto mainAllowListEnd = filterListStructPointer->mainAllowListPointer->end();

    // Check the main allow list.
    while (continueChecking && (mainAllowListEntry != mainAllowListEnd))
    {
        // Get the entry struct.
        EntryStruct *entryStructPointer = *mainAllowListEntry;

        // Check the applied entry against the request URL.
        continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, MAIN_ALLOWLIST, requestStructPointer->urlString,
                                             RequestUrlType::Url, entryStructPointer);

        // Check the applied entry against the request URL with separators.
        if (continueChecking)
            continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, MAIN_ALLOWLIST, requestStructPointer->urlStringWithSeparators,
                                                 RequestUrlType::UrlWithSeparators, entryStructPointer);

        // Increment the main allow list entry.
        ++mainAllowListEntry;
    }

    // Check the initial domain allow list unless a match has already been found.
    if (continueChecking)
    {
        // Get the initial domain allow list iterators.
        auto initialDomainAllowListEntry = filterListStructPointer->initialDomainAllowListPointer->begin();
        auto initialDomainAllowListEnd = filterListStructPointer->initialDomainAllowListPointer->end();

        // Check the initial domain white list.
        while (continueChecking && (initialDomainAllowListEntry != initialDomainAllowListEnd))
        {
            // Get the entry struct.
            EntryStruct *entryStructPointer = *initialDomainAllowListEntry;

            // Check the applied entry against the truncated URL.
            continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, INITIAL_DOMAIN_ALLOWLIST, requestStructPointer->truncatedUrlString,
                                                 RequestUrlType::TruncatedUrl, entryStructPointer);

            //Check the applied entry against the truncated URL with separators.
            if (continueChecking)
                continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, INITIAL_DOMAIN_ALLOWLIST,
                                                     requestStructPointer->truncatedUrlStringWithSeparators, RequestUrlType::TruncatedUrlWithSeparators, entryStructPointer);

            // Increment the initial domain allow list entry.
            ++initialDomainAllowListEntry;
        }
    }

    // Check the main block list unless a match has already been found.
    if (continueChecking)
    {
        // Get the main block list iterators.
        auto mainBlockListEntry = filterListStructPointer->mainBlockListPointer->begin();
        auto mainBlockListEnd = filterListStructPointer->mainBlockListPointer->end();

        // Check the main block list.
        while (continueChecking && (mainBlockListEntry != mainBlockListEnd))
        {
            // Get the entry struct.
            EntryStruct *entryStructPointer = *mainBlockListEntry;

            // Check the applied entry against the request URL.
            continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, MAIN_BLOCKLIST, requestStructPointer->urlString,
                                                 RequestUrlType::Url, entryStructPointer);

            // Check the applied entry against the request URL with separators.
            if (continueChecking)
                continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, MAIN_BLOCKLIST, requestStructPointer->urlStringWithSeparators,
                                                 RequestUrlType::UrlWithSeparators, entryStructPointer);

            // Increment the main block list entry.
            ++mainBlockListEntry;
        }
    }

    // Check the initial domain block list unless a match has already been found.
    if (continueChecking)
    {
        // Get the initial domain block list iterators.
        auto initialDomainBlockListEntry = filterListStructPointer->initialDomainBlockListPointer->begin();
        auto initialDomainBlockListEnd = filterListStructPointer->initialDomainBlockListPointer->end();

        // Check the initial domain block list.
        while (continueChecking && (initialDomainBlockListEntry != initialDomainBlockListEnd))
        {
            // Get the entry struct.
            EntryStruct *entryStructPointer = *initialDomainBlockListEntry;

            // Check the applied entry against the truncated URL.
            continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, INITIAL_DOMAIN_BLOCKLIST, requestStructPointer->truncatedUrlString,
                                                 RequestUrlType::TruncatedUrl, entryStructPointer);

            // Check the applied entry against the truncated URL with separators.
            if (continueChecking)
                continueChecking = checkAppliedEntry(urlRequestInfo, requestStructPointer, filterListStructPointer->title, INITIAL_DOMAIN_BLOCKLIST,
                                                     requestStructPointer->truncatedUrlStringWithSeparators, RequestUrlType::TruncatedUrlWithSeparators, entryStructPointer);

            // Increment the initial domain block list entry.
            ++initialDomainBlockListEntry;
        }
    }

    // Check the regular expression block list unless a match has already been found.
    if (continueChecking)
    {
        // Get the regular expression block list end.
        auto regularExpressionBlockListEntry = filterListStructPointer->regularExpressionBlockListPointer->begin();
        auto regularExpressionBlockListEnd = filterListStructPointer->regularExpressionBlockListPointer->end();

        while (continueChecking && (regularExpressionBlockListEntry != regularExpressionBlockListEnd))
        {
            // Get the entry struct.
            EntryStruct *entryStructPointer = *regularExpressionBlockListEntry;

            // Check the applied entries.
            continueChecking = checkRegularExpression(urlRequestInfo, requestStructPointer, filterListStructPointer->title, REGULAR_EXPRESSION_BLOCKLIST, entryStructPointer);

            // Increment the regular expression block list entry.
            ++regularExpressionBlockListEntry;
        }
    }

    // Return the continue checking status.
    return continueChecking;
}

bool FilterListHelper::checkAppliedEntry(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt, QString urlString,
                                         const RequestUrlType requestUrlType, EntryStruct *entryStructPointer) const
{
    // Check the entries according to the number.
    if (entryStructPointer->singleAppliedEntry)  // There is a single entry.
    {
        // Process initial and final matches.
        if (entryStructPointer->initialMatch && entryStructPointer->finalMatch)  // This is both an initial and final match.
        {
            // Check the URL against the applied entry.
            if (urlString == entryStructPointer->appliedEntryList[0])
            {
                // Set the matched URL type.
                requestStructPointer->matchedUrlType = requestUrlType;

                // Check the domain status.
                return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
            }
        }
        else if (entryStructPointer->initialMatch)  // This is an initial match.
        {
            // Check the URL against the applied entry.
            if (urlString.startsWith(entryStructPointer->appliedEntryList[0]))
            {
                // Set the matched URL type.
                requestStructPointer->matchedUrlType = requestUrlType;

                // Check the domain status.
                return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
            }
        }
        else if (entryStructPointer->finalMatch)  // This is a final match.
        {
            // Check the URL against the applied entry.
            if (urlString.endsWith(entryStructPointer->appliedEntryList[0]))
            {
                // Set the matched URL type.
                requestStructPointer->matchedUrlType = requestUrlType;

                // Check the domain status.
                return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
            }
        }
        else  // There is no initial or final matching.
        {
            // Check if the URL string contains the applied entry.
            if (urlString.contains(entryStructPointer->appliedEntryList[0]))
            {
                // Set the matched URL type.
                requestStructPointer->matchedUrlType = requestUrlType;

                // Check the domain status.
                return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
            }
        }
    }
    else  // There are multiple entries.
    {
        // Process initial and final matches.
        if (entryStructPointer->initialMatch && entryStructPointer->finalMatch)  // This is both an initial and final match.
        {
            // Check the URL.
            if (urlString.startsWith(entryStructPointer->appliedEntryList[0]))
            {
                // Get the number of characters to remove from the front of the URL string.
                int charactersToRemove = entryStructPointer->appliedEntryList[0].size();

                // Remove the entry from the front of the URL string copies.
                urlString.remove(0, charactersToRemove);

                // Get the entry locations.
                int penultimateEntryNumber = (entryStructPointer->sizeOfAppliedEntryList - 1);
                int ultimateEntryIndex = penultimateEntryNumber;

                // Create a URL matches flag.
                bool urlMatches = true;

                // Check all the middle entries.
                for (int i = 1; i < penultimateEntryNumber; ++i)
                {
                    // Get the applied entry index, which will be `-1` if it doesn't exist.
                    int appliedEntryIndex = urlString.indexOf(entryStructPointer->appliedEntryList[i]);

                    // Check if the entry was found.
                    if (appliedEntryIndex >= 0)  // The entry is contained in the URL string.
                    {
                        // Get the number of characters to remove from the front of the URL strings.
                        int charactersToRemove = appliedEntryIndex + entryStructPointer->appliedEntryList[i].size();

                        // Remove the entry from the front of the URL string copies.
                        urlString.remove(0, charactersToRemove);
                    }
                    else  // The entry is not contained in the URL string.
                    {
                        // Mark the URL matches flag as false.
                        urlMatches = false;
                    }
                }

                // Check the final entry if the URL still matches.
                if (urlMatches)
                {
                    if (urlString.endsWith(entryStructPointer->appliedEntryList[ultimateEntryIndex]))  // The URL string ends with the last applied entry.
                    {
                        // Set the matched URL type.
                        requestStructPointer->matchedUrlType = requestUrlType;

                        // Check the domain status.
                        return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
                    }
                }
            }
        }
        else if (entryStructPointer->initialMatch)  // This is an initial match.
        {
            // Check the URL.
            if (urlString.startsWith(entryStructPointer->appliedEntryList[0]))
            {
                // Get the number of characters to remove from the front of the URL strings.
                int charactersToRemove = entryStructPointer->appliedEntryList[0].size();

                // Remove the entry from the front of the URL string copies.
                urlString.remove(0, charactersToRemove);

                // Create a URL matches flag.
                bool urlMatches = true;

                for (int i = 1; i < entryStructPointer->sizeOfAppliedEntryList; ++i)
                {
                    // Get the applied entry index, which will be `-1` if it doesn't exist.
                    int appliedEntryIndex = urlString.indexOf(entryStructPointer->appliedEntryList[i]);

                    // Check if the entry was found.
                    if (appliedEntryIndex >= 0)  // The entry is contained in the URL string.
                    {
                        // Get the number of characters to remove from the front of the URL strings.
                        int charactersToRemove = appliedEntryIndex + entryStructPointer->appliedEntryList[i].size();

                        // Remove the entry from the front of the URL string copies.
                        urlString.remove(0, charactersToRemove);
                    }
                    else  // The entry is not contained in the URL string.
                    {
                        // Mark the URL matches flag as false.
                        urlMatches = false;
                    }
                }

                // Check the domain status if the URL still matches.
                if (urlMatches)
                {
                    // Set the matched URL type.
                    requestStructPointer->matchedUrlType = requestUrlType;

                    // Check the domain status.
                    return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
                }
            }
        }
        else if (entryStructPointer->finalMatch)  // This is a final match.
        {
            // Get the entry locations.
            int penultimateEntryNumber = (entryStructPointer->sizeOfAppliedEntryList - 1);
            int ultimateEntryIndex = penultimateEntryNumber;

            // Create a URL matches flag.
            bool urlMatches = true;

            // Check all the entries except the last one.
            for (int i = 0; i < penultimateEntryNumber; ++i)
            {
                // Get the applied entry index, which will be `-1` if it doesn't exist.
                int appliedEntryIndex = urlString.indexOf(entryStructPointer->appliedEntryList[i]);

                // Check if the entry was found.
                if (appliedEntryIndex >= 0)  // The entry is contained in the URL string.
                {
                    // Get the number of characters to remove from the front of the URL strings.
                    int charactersToRemove = appliedEntryIndex + entryStructPointer->appliedEntryList[i].size();

                    // Remove the entry from the front of the URL string copies.
                    urlString.remove(0, charactersToRemove);
                }
                else  // The entry is not contained in the URL string.
                {
                    // Mark the URL matches flag as false.
                    urlMatches = false;
                }
            }

            // Check the final entry if the URL still matches.
            if (urlMatches)
            {
                if (urlString.endsWith(entryStructPointer->appliedEntryList[ultimateEntryIndex]))  // The URL string ends with the last applied entry.
                {
                    // Set the matched URL type.
                    requestStructPointer->matchedUrlType = requestUrlType;

                    // Check the domain status.
                    return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
                }
            }
        }
        else  // There is no initial or final matching.
        {
            // Create a URL matches flag.
            bool urlMatches = true;

            for (int i = 0; i < entryStructPointer->sizeOfAppliedEntryList; ++i)
            {
                // Get the applied entry index, which will be `-1` if it doesn't exist.
                int appliedEntryIndex = urlString.indexOf(entryStructPointer->appliedEntryList[i]);

                // Check if the entry was found.
                if (appliedEntryIndex >= 0)  // The entry is contained in the URL string.
                {
                    // Get the number of characters to remove from the front of the URL strings.
                    int charactersToRemove = appliedEntryIndex + entryStructPointer->appliedEntryList[i].size();

                    // Remove the entry from the front of the URL string copies.
                    urlString.remove(0, charactersToRemove);
                }
                else  // The entry is not contained in the URL string.
                {
                    // Mark the URL matches flag as false.
                    urlMatches = false;
                }
            }

            // Check the domain status if the URL still matches.
            if (urlMatches)
            {
                // Set the matched URL type.
                requestStructPointer->matchedUrlType = requestUrlType;

                // Check the domain status.
                return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
            }
        }
    }

    // The applied entry doesn't match.  Return `true` to continue processing the URL request.
    return true;
}

bool FilterListHelper::checkRegularExpression(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                                              EntryStruct *entryStructPointer) const
{
    // Create an applied entry regular expression.
    QRegularExpression appliedEntryRegularExpression(entryStructPointer->appliedEntryList[0]);

    // Check if the regular expression matches the applied entry.
    if (requestStructPointer->urlString.contains(appliedEntryRegularExpression))
    {
        // Check the domain status.
        return checkDomain(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
    }

    // If the regular expression doesn't match, return `true` to continue processing the URL request.
    return true;
}

bool FilterListHelper::checkDomain(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                                   EntryStruct *entryStructPointer) const
{
    // Check domain status.
    if (entryStructPointer->domain == FilterOptionDisposition::Null)  // Ignore domain status.
    {
        // Check the third-party status.
        return checkThirdParty(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
    }
    else if (entryStructPointer->domain == FilterOptionDisposition::Apply)  // Block requests from listed domains.
    {
        // Check each domain.
        for (QString blockedDomain : entryStructPointer->domainList)
        {
            // Check if the request came from a blocked domain.
            if (requestStructPointer->firstPartyHost.endsWith(blockedDomain))
            {
                // Check the third-party status.
                return checkThirdParty(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
            }
        }
    }
    else if (entryStructPointer->domain == FilterOptionDisposition::Override)  // Block domains that are not overridden.
    {
        // Create a block domain flag.
        bool blockDomain = true;

        // Check each overridden domain.
        for (QString overriddenDomain : entryStructPointer->domainList)
        {
            // Check if the request came from an overridden domain.
            if (requestStructPointer->firstPartyHost.endsWith(overriddenDomain))
            {
                // Don't block the domain.
                blockDomain = false;
            }
        }

        // Continue checking if the domain is blocked.
        if (blockDomain)
        {
            // Check the third-party status.
            return checkThirdParty(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
    }

    // There is a domain specified that doesn't match this request.  Return `true` to continue processing the URL request.
    return true;
}

bool FilterListHelper::checkThirdParty(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                                       EntryStruct *entryStructPointer) const
{
    // Check third-party status.
    if (entryStructPointer->thirdParty == FilterOptionDisposition::Null)  // Ignore third-party status.
    {
        // Check if request options are applied.
        if (entryStructPointer->hasRequestOptions)  // Request options are applied.
        {
            // Check the request options.
            return checkRequestOptions(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
        else  // Request options are not applied.
        {
            // Process the request.
            return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
    }
    else if ((entryStructPointer->thirdParty == FilterOptionDisposition::Apply) && requestStructPointer->isThirdPartyRequest)  // Block third-party request.
    {
        // Check if request options are applied.
        if (entryStructPointer->hasRequestOptions)  // Request options are applied.
        {
            // Check the request options.
            return checkRequestOptions(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
        else  // Request options are not applied.
        {
            // Process the request.
            return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
    }
    else if ((entryStructPointer->thirdParty == FilterOptionDisposition::Override) && !requestStructPointer->isThirdPartyRequest)  // Block first-party requests.
    {
        // Check if request options are applied.
        if (entryStructPointer->hasRequestOptions)  // Request options are applied.
        {
            // Check the request options.
            return checkRequestOptions(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
        else  // Request options are not applied.
        {
            // Process the request.
            return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);
        }
    }

    // The third-party option specified doesn't match this request.  Return `true` to continue processing the URL request.
    return true;
}

bool FilterListHelper::checkRequestOptions(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                                           EntryStruct *entryStructPointer) const
{
    // Process font requests.
    if ((entryStructPointer->font == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeFontResource))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process image requests.
    if ((entryStructPointer->image == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeImage))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process main frame requests.
    if ((entryStructPointer->mainFrame == FilterOptionDisposition::Apply) && ((requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeMainFrame) ||
                                                                              (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeNavigationPreloadMainFrame)))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process media requests.
    if ((entryStructPointer->media == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeMedia))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process object requests.
    if ((entryStructPointer->object == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeObject))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process other requests.
    if ((entryStructPointer->other == FilterOptionDisposition::Apply) && ((requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeSubResource) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeWorker) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeSharedWorker) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypePrefetch) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeFavicon) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeServiceWorker) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeCspReport) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypePluginResource) ||
                                                                          (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeUnknown)))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process ping requests
    if ((entryStructPointer->ping == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypePing))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process script requests.
    if ((entryStructPointer->script == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeScript))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process style sheet requests.
    if ((entryStructPointer->styleSheet == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeStylesheet))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process sub resource requests.
    if ((entryStructPointer->subFrame == FilterOptionDisposition::Apply) && ((requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeSubFrame) ||
                                                                             (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeNavigationPreloadSubFrame)))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // Process XML HTTP requests.
    if ((entryStructPointer->xmlHttpRequest == FilterOptionDisposition::Apply) && (requestStructPointer->resourceTypeInt == QWebEngineUrlRequestInfo::ResourceTypeXhr))
        return processRequest(urlRequestInfo, requestStructPointer, filterListTitle, sublistInt, entryStructPointer);

    // The request options specified don't match this request.  Return `true` to continue processing the URL request.
    return true;
}

bool FilterListHelper::processRequest(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                                    EntryStruct *entryStructPointer) const
{
    // Set the request disposition.
    if (sublistInt <= 2)
        requestStructPointer->dispositionInt = ALLOWED;
    else
        requestStructPointer->dispositionInt = BLOCKED;

    // Populate the request struct.
    requestStructPointer->filterListTitle = filterListTitle;
    requestStructPointer->sublistInt = sublistInt;
    requestStructPointer->entryStruct = *entryStructPointer;

    // Block the request if specified.
    if (requestStructPointer->dispositionInt == BLOCKED)
        urlRequestInfo.block(true);

    // Log the disposition.
    //qDebug().noquote().nospace() << "Request processed:  " << urlRequestInfo.firstPartyUrl() << ",  Filter list entry:  " << entryStructPointer->appliedEntry;

    // Returning `false` stops all processing of the request.
    return false;
}

QString FilterListHelper::getDispositionString(int dispositionInt) const
{
    // Return the translated disposition string.
    switch (dispositionInt)
    {
        case ALLOWED: return ALLOWED_STRING;
        case BLOCKED: return BLOCKED_STRING;
        default: return DEFAULT_STRING;
    }
}

QString FilterListHelper::getNavigationTypeString(int navigationTypeInt) const
{
    // Return the translated navigation type string.
    switch (navigationTypeInt)
    {
        case QWebEngineUrlRequestInfo::NavigationTypeLink: return NAVIGATION_TYPE_LINK;
        case QWebEngineUrlRequestInfo::NavigationTypeTyped: return NAVIGATION_TYPE_TYPED;
        case QWebEngineUrlRequestInfo::NavigationTypeFormSubmitted: return NAVIGATION_TYPE_FORM_SUBMITTED;
        case QWebEngineUrlRequestInfo::NavigationTypeBackForward: return NAVIGATION_TYPE_BACK_FORWARD;
        case QWebEngineUrlRequestInfo::NavigationTypeReload: return NAVIGATION_TYPE_RELOAD;
        case QWebEngineUrlRequestInfo::NavigationTypeRedirect: return NAVIGATION_TYPE_REDIRECT;
        default: return NAVIGATION_TYPE_OTHER;
    }
}

QString FilterListHelper::getRequestOptionDispositionString(const FilterOptionDisposition filterOptionDisposition) const
{
    // Return the translated filter option disposition string.
    switch (filterOptionDisposition)
    {
        case FilterOptionDisposition::Apply: return FILTER_OPTION_APPLY;
        case FilterOptionDisposition::Override: return FILTER_OPTION_OVERRIDE;
        default: return FILTER_OPTION_NULL;
    }
}

QString FilterListHelper::getResourceTypeString(int resourceTypeInt) const
{
    // Return the translated resource type string.
    switch (resourceTypeInt)
    {
        case QWebEngineUrlRequestInfo::ResourceTypeMainFrame: return RESOURCE_TYPE_MAIN_FRAME;
        case QWebEngineUrlRequestInfo::ResourceTypeSubFrame: return RESOURCE_TYPE_SUB_FRAME;
        case QWebEngineUrlRequestInfo::ResourceTypeStylesheet: return RESOURCE_TYPE_STYLESHEET;
        case QWebEngineUrlRequestInfo::ResourceTypeScript: return RESOURCE_TYPE_SCRIPT;
        case QWebEngineUrlRequestInfo::ResourceTypeImage: return RESOURCE_TYPE_IMAGE;
        case QWebEngineUrlRequestInfo::ResourceTypeFontResource: return RESOURCE_TYPE_FONT_RESOURCE;
        case QWebEngineUrlRequestInfo::ResourceTypeSubResource: return RESOURCE_TYPE_SUB_RESOURCE;
        case QWebEngineUrlRequestInfo::ResourceTypeObject: return RESOURCE_TYPE_OBJECT;
        case QWebEngineUrlRequestInfo::ResourceTypeMedia: return RESOURCE_TYPE_MEDIA;
        case QWebEngineUrlRequestInfo::ResourceTypeWorker: return RESOURCE_TYPE_WORKER;
        case QWebEngineUrlRequestInfo::ResourceTypeSharedWorker: return RESOURCE_TYPE_SHARED_WORKER;
        case QWebEngineUrlRequestInfo::ResourceTypePrefetch: return RESOURCE_TYPE_PREFETCH;
        case QWebEngineUrlRequestInfo::ResourceTypeFavicon: return RESOURCE_TYPE_FAVICON;
        case QWebEngineUrlRequestInfo::ResourceTypeXhr: return RESOURCE_TYPE_XHR;
        case QWebEngineUrlRequestInfo::ResourceTypePing: return RESOURCE_TYPE_PING;
        case QWebEngineUrlRequestInfo::ResourceTypeServiceWorker: return RESOURCE_TYPE_SERVICE_WORKER;
        case QWebEngineUrlRequestInfo::ResourceTypeCspReport: return RESOURCE_TYPE_CSP_REPORT;
        case QWebEngineUrlRequestInfo::ResourceTypePluginResource: return RESOURCE_TYPE_PLUGIN_RESOURCE;
        case QWebEngineUrlRequestInfo::ResourceTypeNavigationPreloadMainFrame: return RESOURCE_TYPE_NAVIGATION_PRELOAD_MAIN_FRAME;
        case QWebEngineUrlRequestInfo::ResourceTypeNavigationPreloadSubFrame: return RESOURCE_TYPE_NAVIGATION_PRELOAD_SUB_FRAME;
        default: return RESOURCE_TYPE_UNKNOWN;
    }
}

QString FilterListHelper::getSublistName(int sublistInt) const
{
    // Return the name of the requested sublist.
    switch (sublistInt)
    {
        case MAIN_ALLOWLIST: return MAIN_ALLOWLIST_STRING;
        case INITIAL_DOMAIN_ALLOWLIST: return INITIAL_DOMAIN_ALLOWLIST_STRING;
        case REGULAR_EXPRESSION_ALLOWLIST: return REGULAR_EXPRESSION_ALLOWLIST_STRING;
        case MAIN_BLOCKLIST: return MAIN_BLOCKLIST_STRING;
        case INITIAL_DOMAIN_BLOCKLIST: return INITIAL_DOMAIN_BLOCKLIST_STRING;
        case REGULAR_EXPRESSION_BLOCKLIST: return REGULAR_EXPRESSION_BLOCKLIST_STRING;
        default: return QString();  // The default return should never be reached.
    }
}

FilterListStruct* FilterListHelper::populateFilterList(const QString &filterListFileName) const
{
    // Get the filter list file.
    QFile filterListFile(filterListFileName);

    // Open the filter list file.
    filterListFile.open(QIODevice::ReadOnly);

    // Create a filter list text stream.
    QTextStream filterListTextStream(&filterListFile);

    // Create a filter list struct.
    FilterListStruct *filterListStructPointer = new FilterListStruct;

    // Populate the filter list file name.
    filterListStructPointer->filePath = filterListFileName;

    // Create a filter list string.
    QString filterListString;

    // Process each line of the filter list.
    while (filterListTextStream.readLineInto(&filterListString)) {
        // Create an entry struct.
        EntryStruct *entryStructPointer = new EntryStruct;

        // Store the original entry.
        entryStructPointer->originalEntry = filterListString;

        // Process the entry.
        if (filterListString.isEmpty())  // Ignore empty lines.
        {
            // Do nothing.

            // Log the dropping of the line.
            //qDebug().noquote().nospace() << filterListString << " NOT added from " << filterListFileName << " (empty line).";
        }
        else if (filterListString.startsWith(QLatin1Char('[')))  // The line starts with `[`, which is the file format.
        {
            // Do nothing.

            // Log the dropping of the line.
            //qDebug().noquote().nospace() << filterListString << " NOT added from " << filterListFileName << " (file format).";
        }
        else if (filterListString.contains(QLatin1String("##")) ||
                 filterListString.contains(QLatin1String("#?#")) ||
                 filterListString.contains(QLatin1String("#@#")) ||
                 filterListString.contains(QLatin1String("#$#")))  // The line contains unimplemented content filtering.
        {
            // Do nothing.

            // Log the dropping of the line.
            //qDebug().noquote().nospace() << filterListString << " NOT added from " << filterListFileName << " (content filtering).";
        }
        else if (filterListString.startsWith(QLatin1Char('!')))  // The line starts with `!`, which are comments.
        {
            if (filterListString.startsWith(QLatin1String("! Title: ")))  // The line contains the title.
            {
                // Add the title to the filter list struct.
                filterListStructPointer->title = filterListString.remove(0, 9);

                // Log the addition of the filter list title.
                //qDebug().noquote().nospace() << "Filter list title:  " << filterListString << " added from " << filterListFileName;
            }
            else if (filterListString.startsWith(QLatin1String("! Version: ")))  // The line contains the version.
            {
                // Add the version to the filter list struct.
                filterListStructPointer->version = filterListString.remove(0, 11);

                // Log the addition of the filter list version.
                //qDebug().noquote().nospace() << "Filter list version:  " << filterListString << " added from " << filterListFileName;
            }

            // Else do nothing.

            // Log the dropping of the line.
            //qDebug().noquote().nospace() << originalFilterListString << " NOT added from " << filterListFileName;
        }
        else  // Process the filter options.
        {
            // Get the index of the last dollar sign.
            int indexOfLastDollarSign = filterListString.lastIndexOf(QLatin1Char('$'));

            // Process the filter options if they exist.
            if (indexOfLastDollarSign > -1)
            {
                // Get the filter options.
                entryStructPointer->originalFilterOptions = filterListString.section(QLatin1Char('$'), -1);

                // Store the entry without the filter options as the filter list string.
                filterListString.truncate(indexOfLastDollarSign);

                // Split the filter options.
                QStringList originalFilterOptionsList = entryStructPointer->originalFilterOptions.split(QLatin1Char(','));

                // Create an applied filter options list.
                QStringList appliedFilterOptionsList;

                // Populate the applied filter options list.
                for (QString filterOption : originalFilterOptionsList)
                {
                    // Only add filter options that are handled by Privacy Browser.  <https://help.adblockplus.org/hc/en-us/articles/360062733293-How-to-write-filters>
                    // `csp` specifies a Content Security Policy, which isn't handled by Privacy Browser.
                    // `method` appears to specify the request method, but that isn't an official filter option, and Privacy Browser has chosen to ignore it.
                    // `redirect` is probably a misspelling of the `rewrite` option.
                    // `rewrite` would be difficult to implement, so it isn't used by Privacy Browser.
                    // `1p` is not an official filter option.
                    // `elemhide` isn't implemented by Privacy Browser.
                    // `genericblock` isn't implemented by Privacy Browser.
                    // `generichide` isn't implemented by Privacy Browser.
                    // `match-case` isn't currently implemented by Privacy Browser, but it could be if that became important.
                    // `xhr` is probably a misspelling of `xmlhttprequest`.
                    if (!(filterOption.startsWith(QLatin1String("csp=")) ||
                          filterOption.startsWith(QLatin1String("method=")) ||
                          filterOption.startsWith(QLatin1String("redirect=")) ||
                          filterOption.startsWith(QLatin1String("rewrite=")) ||
                          (filterOption == (QLatin1String("1p"))) ||
                          (filterOption == (QLatin1String("elemhide"))) ||
                          (filterOption == (QLatin1String("genericblock"))) ||
                          (filterOption == (QLatin1String("generichide"))) ||
                          (filterOption == (QLatin1String("match-case"))) ||
                          (filterOption == (QLatin1String("xhr")))))
                        appliedFilterOptionsList.append(filterOption);
                }

                // Store the applied filter options list.
                entryStructPointer->appliedFilterOptionsList = appliedFilterOptionsList;

                // Initialize an override struct.
                OverrideStruct overrideStruct;

                // Populate the filter options entries.
                for (QString filterOption : appliedFilterOptionsList)
                {
                    // Parse the filter options.
                    if (filterOption.startsWith(QLatin1String("domain=")))  // Domain.
                    {
                        // Remove `domain=` from the filter option.
                        filterOption.remove(0, 7);

                        // Store the domain list.
                        entryStructPointer->domainList = filterOption.split(QLatin1Char('|'));

                        // Set the disposition.
                        if (entryStructPointer->domainList[0].startsWith(QLatin1Char('~')))  // Override domains.
                        {
                            // Populate the domain filter disposition.
                            entryStructPointer->domain = FilterOptionDisposition::Override;

                            // Remove the initial `~` from each domain.
                            entryStructPointer->domainList.replaceInStrings(QLatin1String("~"), QLatin1String(""));
                        }
                        else  // Standard domains.
                        {
                            // Populate the domain filter disposition.
                            entryStructPointer->domain = FilterOptionDisposition::Apply;
                        }
                    }
                    else if (filterOption == QLatin1String("third-party"))  // Third-party.
                    {
                        // Populate the third-party filter disposition.
                        entryStructPointer->thirdParty = FilterOptionDisposition::Apply;
                    }
                    else if (filterOption == QLatin1String("~third-party"))  // Third-party override.
                    {
                        // Populate the third-party filter disposition.
                        entryStructPointer->thirdParty = FilterOptionDisposition::Override;
                    }
                    else if ((filterOption == QLatin1String("document")) || (filterOption == QLatin1String("popup")))  // Document (and popup).
                    {
                        // Populate the main frame disposition.
                        entryStructPointer->mainFrame = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("font"))  // Font.
                    {
                        // Populate the font disposition.
                        entryStructPointer->font = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("image"))  // Image.
                    {
                        // Populate the image disposition.
                        entryStructPointer->image = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("media"))  // Media.
                    {
                        // Populate the media disposition.
                        entryStructPointer->media = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("object"))  // Object.
                    {
                        // Populate the object disposition.
                        entryStructPointer->object = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if ((filterOption == QLatin1String("other")) || (filterOption == QLatin1String("webrtc")) || (filterOption == QLatin1String("websocket")))  // Other.
                    {  // `websocket` will get its own section in Qt6.
                        // Populate the other disposition.
                        entryStructPointer->other = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("ping"))  // Ping.
                    {
                        // Populate the ping disposition.
                        entryStructPointer->ping = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("script"))  // Script.
                    {
                        // Populate the script disposition.
                        entryStructPointer->script = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("stylesheet"))  // Style sheet.
                    {
                        // Populate the script disposition.
                        entryStructPointer->styleSheet = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("subdocument"))  // Sub document.
                    {
                        // Populate the sub resource disposition.
                        entryStructPointer->subFrame = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("xmlhttprequest"))  // XML HTTP request.
                    {
                        //Populate the XML HTTP request disposition.
                        entryStructPointer->xmlHttpRequest = FilterOptionDisposition::Apply;

                        // Set the has request options flag.
                        entryStructPointer->hasRequestOptions = true;
                    }
                    else if (filterOption == QLatin1String("~document"))  // Document override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.mainFrame = true;
                    }
                    else if (filterOption == QLatin1String("~font"))  // Font override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.font = true;
                    }
                    else if (filterOption == QLatin1String("~image"))  // Image override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.image = true;
                    }
                    else if (filterOption == QLatin1String("~media"))  // Media override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.media = true;
                    }
                    else if (filterOption == QLatin1String("~object"))  // Object override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.object = true;
                    }
                    else if ((filterOption == QLatin1String("~other")) || (filterOption == QLatin1String("~webrtc")) || (filterOption == QLatin1String("~websocket")))  // Other override.
                    {  // `websocket` will get its own section in Qt6.
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.other = true;
                    }
                    else if (filterOption == QLatin1String("~ping"))  // Ping override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.ping = true;
                    }
                    else if (filterOption == QLatin1String("~script"))  // Script override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.script = true;
                    }
                    else if (filterOption == QLatin1String("~stylesheet"))  // Style sheet override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.styleSheet = true;
                    }
                    else if (filterOption == QLatin1String("~subdocument"))  // Sub document override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.subFrame = true;
                    }
                    else if (filterOption == QLatin1String("~xmlhttprequest"))  // XML HTTP request override.
                    {
                        // Populate the override struct.
                        overrideStruct.hasOverride = true;
                        overrideStruct.xmlHttpRequest = true;
                    }
                }

                // Apply the overrides.
                if (overrideStruct.hasOverride)
                {
                    // Set the has request options flag.
                    entryStructPointer->hasRequestOptions = true;

                    // Font.
                    if (overrideStruct.font)
                        entryStructPointer->font = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->font = FilterOptionDisposition::Apply;

                    // Image.
                    if (overrideStruct.image)
                        entryStructPointer->image = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->image = FilterOptionDisposition::Apply;

                    // Main Frame (document).
                    if (overrideStruct.mainFrame)
                        entryStructPointer->mainFrame = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->mainFrame = FilterOptionDisposition::Apply;

                    // Media.
                    if (overrideStruct.media)
                        entryStructPointer->media = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->media = FilterOptionDisposition::Apply;

                    // Object.
                    if (overrideStruct.object)
                        entryStructPointer->object = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->object = FilterOptionDisposition::Apply;

                    // Other.
                    if (overrideStruct.other)
                        entryStructPointer->other = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->other = FilterOptionDisposition::Apply;

                    // Ping.
                    if (overrideStruct.ping)
                        entryStructPointer->ping = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->ping = FilterOptionDisposition::Apply;

                    //  Script.
                    if (overrideStruct.script)
                        entryStructPointer->script = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->script = FilterOptionDisposition::Apply;

                    // Style Sheet.
                    if (overrideStruct.styleSheet)
                        entryStructPointer->styleSheet = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->styleSheet = FilterOptionDisposition::Apply;

                    // Sub Resource.
                    if (overrideStruct.subFrame)
                        entryStructPointer->subFrame = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->subFrame = FilterOptionDisposition::Apply;

                    // XML HTTP Request.
                    if (overrideStruct.xmlHttpRequest)
                        entryStructPointer->xmlHttpRequest = FilterOptionDisposition::Override;
                    else
                        entryStructPointer->xmlHttpRequest = FilterOptionDisposition::Apply;
                }
            }  // Finish processing filter options.


            if (filterListString.isEmpty() && !entryStructPointer->hasRequestOptions)  // There are no applied entries and no request options.
            {
                // Ignore these entries as they will block all requests generally or for a specified domain.  Typically these are left over after removing `csp=` filter options.

                // Log the dropping of the entry.
                //qDebug().noquote().nospace() << entryStructPointer->originalEntry << " NOT added from " << filterListFileName << ".";
            }
            else if (filterListString.startsWith(QLatin1String("@@")))  // Process an allow list entry.
            {
                // Remove the initial `@@`.
                filterListString.remove(0, 2);

                if (filterListString.startsWith(QLatin1String("||")))  // Process an initial domain allow list entry.
                {
                    // Remove the initial `||`.
                    filterListString.remove(0, 2);

                    // Set the initial match flag.
                    entryStructPointer->initialMatch = true;

                    // Prepare the filter list string.
                    prepareFilterListString(filterListString, entryStructPointer);

                    // Add the entry struct to the initial domain allow list.
                    filterListStructPointer->initialDomainAllowListPointer->push_front(entryStructPointer);

                    // Log the addition to the filter list.
                    //qDebug().noquote().nospace() << entryStructPointer->originalEntry << " added to Initial Domain Allow List from " << filterListFileName << ".";
                }
                else if (filterListString.contains(QLatin1String("\\")))  // Process a regular expression allow list entry.
                {
                    // Add the regular expression to the applied entry list.
                    entryStructPointer->appliedEntryList.append(filterListString);

                    // Add the entry struct to the regular expression allow list.
                    filterListStructPointer->regularExpressionAllowListPointer->push_front(entryStructPointer);
                }
                else  // Process a main allow list entry.
                {
                    // Prepare the filter list string.
                    prepareFilterListString(filterListString, entryStructPointer);

                    // Add the entry struct to the main allow list.
                    filterListStructPointer->mainAllowListPointer->push_front(entryStructPointer);

                    // Log the addition to the filter list.
                    //qDebug().noquote().nospace() << entryStructPointer->originalEntry << " added to Main Allow List from " << filterListFileName << ".";
                }
            }
            else if (filterListString.startsWith(QLatin1String("||")))  // Process an initial domain block list entry.
            {
                // Remove the initial `||`.
                filterListString.remove(0, 2);

                // Set the initial match flag.
                entryStructPointer->initialMatch = true;

                // Prepare the filter list string.
                prepareFilterListString(filterListString, entryStructPointer);

                // Add the entry struct to the initial domain block list.
                filterListStructPointer->initialDomainBlockListPointer->push_front(entryStructPointer);

                // Log the addition to the filter list.
                //qDebug().noquote().nospace() << entryStructPointer->originalEntry << " added to Initial Domain Block List from " << filterListFileName << ".";
            }
            else if (filterListString.contains(QLatin1String("\\")))  // Process a regular expression block list entry.
            {
                // Add the regular expression to the applied entry list.
                entryStructPointer->appliedEntryList.append(filterListString);

                // Add the entry struct to the regular expression block list.
                filterListStructPointer->regularExpressionBlockListPointer->push_front(entryStructPointer);
            }
            else  // Process a main block list entry.
            {
                // Prepare the filter list string.
                prepareFilterListString(filterListString, entryStructPointer);

                // Add the entry struct to the main block list.
                filterListStructPointer->mainBlockListPointer->push_front(entryStructPointer);

                // Log the addition to the filter list.
                //qDebug().noquote().nospace() << entryStructPointer->originalEntry << " added to Main Block List from " << filterListFileName << ".";
            }
        }
    }

    // Close the filter list file.
    filterListFile.close();

    // Return the filter list pair.
    return filterListStructPointer;
}

void FilterListHelper::prepareFilterListString(QString &filterListString, EntryStruct *entryStructPointer) const
{
    // Check if this is an initial match.
    if (filterListString.startsWith(QLatin1Char('|')))
    {
        // Strip the initial `|`.
        filterListString.remove(0, 1);

        // Set the initial match flag.
        entryStructPointer->initialMatch = true;
    }

    // Check if this is a final match.
    if (filterListString.endsWith(QLatin1Char('|')))
    {
        // Strip the final `|`.
        filterListString.chop(1);

        // Set the final match flag.
        entryStructPointer->finalMatch = true;
    }

    // Remove the initial asterisk if it exists.
    if (filterListString.startsWith(QLatin1Char('*')))
        filterListString.remove(0, 1);

    // Remove the final asterisk if it exists.
    if (filterListString.endsWith(QLatin1Char('*')))
        filterListString.chop(1);

    // Split the filter list string and set it as the applied entry list.
    entryStructPointer->appliedEntryList = filterListString.split(QLatin1Char('*'));

    // Store the size of the applied entry list.
    entryStructPointer->sizeOfAppliedEntryList = entryStructPointer->appliedEntryList.size();

    // Determine if this is a single applied entry (including an empty entry).
    entryStructPointer->singleAppliedEntry = (entryStructPointer->sizeOfAppliedEntryList == 1);
}
