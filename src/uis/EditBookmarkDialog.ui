<?xml version="1.0" encoding="UTF-8"?>

<!--
  SPDX-License-Identifier: GPL-3.0-or-later
  SPDX-FileCopyrightText: 2023 Soren Stoutner <soren@stoutner.com>

  This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <https://www.gnu.org/licenses/>. -->

<ui version="4.0">
    <class>EditBookmarkDialog</class>

    <widget class="QWidget">
        <layout class="QVBoxLayout">
            <!-- Dialog body. -->
            <item>
                <layout class="QHBoxLayout">
                    <!-- Favorite icon column. -->
                    <item>
                        <layout class="QVBoxLayout">
                            <property name="rightMargin">
                                <number>10</number>
                            </property>

                            <!-- First row, current favorite icon. -->
                            <item>
                                <widget class="QRadioButton" name="currentFavoriteIconRadioButton">
                                    <property name="text">
                                        <string>Current favorite icon</string>
                                    </property>

                                    <property name="checked">
                                        <bool>true</bool>
                                    </property>

                                    <property name="iconSize">
                                        <size>
                                            <height>32</height>
                                            <width>32</width>
                                        </size>
                                    </property>
                                </widget>
                            </item>

                            <!-- Second row, current website favorite icon. -->
                            <item>
                                <widget class="QRadioButton" name="currentWebsiteFavoriteIconRadioButton">
                                    <property name="text">
                                        <string>Current website favorite icon</string>
                                    </property>

                                    <property name="iconSize">
                                        <size>
                                            <height>32</height>
                                            <width>32</width>
                                        </size>
                                    </property>
                                </widget>
                            </item>

                            <!-- Third row, custom favorite icon. -->
                            <item>
                                <widget class="QRadioButton" name="customFavoriteIconRadioButton">
                                    <property name="text">
                                        <string>Custom favorite icon</string>
                                    </property>

                                    <property name="iconSize">
                                        <size>
                                            <height>32</height>
                                            <width>32</width>
                                        </size>
                                    </property>
                                </widget>
                            </item>

                            <!-- Spacer. -->
                            <item>
                                <spacer>
                                    <property name="orientation">
                                        <enum>Qt::Vertical</enum>
                                    </property>
                                </spacer>
                            </item>
                        </layout>
                    </item>

                    <!-- Parent folder, name and URL column. -->
                    <item>
                        <layout class="QVBoxLayout">
                            <!-- First row. -->
                            <item>
                                <layout class="QHBoxLayout">
                                    <property name="alignment">
                                        <enum>Qt::AlignLeft</enum>
                                    </property>

                                    <!-- Parent folder.  -->
                                    <item>
                                        <widget class="QTreeWidget" name="parentFolderTreeWidget">
                                            <property name="minimumSize">
                                                <size>
                                                    <width>1000</width>
                                                    <height>700</height>
                                                </size>
                                            </property>
                                        </widget>
                                    </item>
                                </layout>
                            </item>

                            <!-- Second row. -->
                            <item>
                                <layout class="QHBoxLayout">
                                    <property name="alignment">
                                        <enum>Qt::AlignLeft</enum>
                                    </property>

                                    <!-- Bookmark name.  -->
                                    <item>
                                        <widget class="QLabel">
                                            <property name="toolTip">
                                                <string>The name of the bookmark.</string>
                                            </property>

                                            <property name="text">
                                                <string>Bookmark name</string>
                                            </property>
                                        </widget>
                                    </item>

                                    <item>
                                        <widget class="QLineEdit" name="bookmarkNameLineEdit" />
                                    </item>
                                </layout>
                            </item>

                            <!-- Second row. -->
                            <item>
                                <layout class="QHBoxLayout">
                                    <property name="alignment">
                                        <enum>Qt::AlignLeft</enum>
                                    </property>

                                    <!-- Bookmark URL. -->
                                    <item>
                                        <widget class="QLabel">
                                            <property name="toolTip">
                                                <string>The URL of the bookmark.</string>
                                            </property>

                                            <property name="text">
                                                <string>Bookmark URL</string>
                                            </property>
                                        </widget>
                                    </item>

                                    <item>
                                        <widget class="QLineEdit" name="bookmarkUrlLineEdit">
                                            <property name="sizePolicy">
                                                <sizepolicy hsizetype="MinimumExpanding" vsizetype="Fixed">
                                                    <horstretch>0</horstretch>
                                                    <verstretch>0</verstretch>
                                                </sizepolicy>
                                            </property>

                                            <property name="minimumSize">
                                                <size>
                                                    <width>700</width>
                                                    <height>0</height>
                                                </size>
                                            </property>
                                        </widget>
                                    </item>
                                </layout>
                            </item>
                        </layout>
                    </item>
                </layout>
            </item>

            <!-- Buttons. -->
            <item>
                <layout class="QHBoxLayout">
                    <!-- Browse button. -->
                    <item>
                        <widget class="QPushButton" name="browseButton">
                            <property name="text">
                                <string>Browse</string>
                            </property>

                            <property name="icon">
                                <iconset theme="insert-image" />
                            </property>
                        </widget>
                    </item>

                    <!-- Cancel button - dialog button box. -->
                    <item>
                        <widget class="QDialogButtonBox" name="dialogButtonBox">
                            <property name="standardButtons">
                                <set>QDialogButtonBox::Save | QDialogButtonBox::Cancel</set>
                            </property>
                        </widget>
                    </item>
                </layout>
            </item>
        </layout>
    </widget>
</ui>
