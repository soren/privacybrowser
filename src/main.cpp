/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2025 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "GlobalVariables.h"
#include "databases/BookmarksDatabase.h"
#include "databases/CookiesDatabase.h"
#include "databases/DomainsDatabase.h"
#include "helpers/FilterListHelper.h"
#include "windows/BrowserWindow.h"

// KDE Frameworks headers.
#include <KAboutData>
#include <KCrash>
#include <KDBusService>
#include <KLocalizedString>

// Qt headers.
#include <QApplication>
#include <QCommandLineParser>
#include <QFile>

// Declare the global variables.
bool globalChromiumInstalled;
FilterListHelper *globalFilterListHelperPointer;
bool globalFirefoxInstalled;

int main(int argc, char *argv[])
{
    // Create the application.
    QApplication application(argc, argv);

    // Set the localization application domain.
    KLocalizedString::setApplicationDomain("privacybrowser");

    // Initialize KCrash.
    KCrash::initialize();

    // Instantiate about data, setting the component name, the display name, and the version.
    KAboutData aboutData(QLatin1String("privacybrowser"), i18nc("Program Name", "Privacy Browser"), QLatin1String("0.8"));

    // Add the author name, job description, email address, and website.
    aboutData.addAuthor(i18nc("Developer Information", "Soren Stoutner"),i18nc("Developer Information", "Principal developer"), QLatin1String("soren@stoutner.com"),
                        QLatin1String("https://www.stoutner.com/"));

    // Populate additional about data info.
    aboutData.setBugAddress("https://redmine.stoutner.com/projects/privacy-browser-pc/issues");
    aboutData.setCopyrightStatement(i18nc("Copyright", "Copyright 2016-2017, 2021-2025 Soren Stoutner <soren@stoutner.com>"));
    aboutData.setDesktopFileName(QLatin1String("com.stoutner.privacybrowser"));
    aboutData.setHomepage(QLatin1String("https://www.stoutner.com/privacy-browser-pc/"));
    //aboutData.setLicense(KAboutLicense::GPL_V3, KAboutLicense::OrLaterVersions);  <https://redmine.stoutner.com/issues/822>
    aboutData.setLicenseTextFile(QLatin1String(":/licenses/GPLv3+.txt"));
    aboutData.setOrganizationDomain("stoutner.com");
    aboutData.setShortDescription(i18nc("Tagline", "A web browser that respects your privacy."));

    // Set the application data.
    KAboutData::setApplicationData(aboutData);

    // Set the window icon.
    application.setWindowIcon(QIcon::fromTheme(QLatin1String("privacy-browser"), QIcon(QLatin1String(":/icons/sc-apps-privacybrowser.svg"))));

    // Create a command line parser.
    QCommandLineParser commandLineParser;

    // Process the command line through about data (this adds --license and --author).
    aboutData.setupCommandLine(&commandLineParser);

    // Process the application command line options (this adds --version, --help, and --help-all).
    commandLineParser.process(application);

    // Make it so.
    aboutData.processCommandLine(&commandLineParser);

    // Register with D-Bus, allowing multiple instances and allowing the program to run if for some reason the registration fails.
    KDBusService appDBusService(KDBusService::Multiple | KDBusService::NoExitOnFailure);

    // Create the app data location directory if it doesn't currently exist.  This directory is used to store the databases in the subsequent commands.
    // The first directory in the list should be the private, writable location, which on Linux should be `/home/user/.local/share/privacybrowser`.
    QDir().mkdir(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first());

    // Add the databases.
    BookmarksDatabase::addDatabase();
    CookiesDatabase::addDatabase();
    DomainsDatabase::addDatabase();

    // Populate the global filter list helper.
    globalFilterListHelperPointer = new FilterListHelper;

    // Check if other browsers are installed and store the result in the global variables
    globalChromiumInstalled = (system("chromium --version > /dev/null 2> /dev/null") == 0);
    globalFirefoxInstalled = (system("firefox -v > /dev/null 2> /dev/null") == 0);

    // Create the main window.
    BrowserWindow *browserWindowPointer = new BrowserWindow();

    // Show the main window.
    browserWindowPointer->show();

    // Return the application.
    return application.exec();
}
