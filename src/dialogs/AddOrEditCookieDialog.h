/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022, 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADDOREDITCOOKIEDIALOG_H
#define ADDOREDITCOOKIEDIALOG_H

// Qt toolkit headers.
#include <QCheckBox>
#include <QDateTimeEdit>
#include <QDialog>
#include <QNetworkCookie>

class AddOrEditCookieDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit AddOrEditCookieDialog(QWidget *parentWidgetPointer, const int dialogType, const QNetworkCookie *cookiePointer = nullptr, const bool isDurable = false);

    // The public static constants.
    static const int AddCookie;
    static const int EditCookie;

Q_SIGNALS:
    // The signals.
    void addCookie(const QNetworkCookie &cookie, const bool &isDurable) const;
    void deleteCookie(const QNetworkCookie &cookie) const;

private Q_SLOTS:
    // The private slots.
    void saveCookie();
    void updateExpirationDateTimeState(const int &newState) const;
    void updateUi() const;

private:
    // The private widgets.
    QLineEdit *domainLineEditPointer;
    QCheckBox *durableCheckBoxPointer;
    QCheckBox *expirationCheckBoxPointer;
    QDateTimeEdit *expirationDateTimeEditPointer;
    QCheckBox *httpOnlyCheckBoxPointer;
    QLineEdit *nameLineEditPointer;
    QLineEdit *pathLineEditPointer;
    QPushButton *saveButtonPointer;
    QCheckBox *secureCheckBoxPointer;
    QLineEdit *valueLineEditPointer;

    // The private variables.
    bool isEditDialog;
    QNetworkCookie originalCookie;
    bool originalIsDurable;
};
#endif
