/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

// Application headers.
#include "HttpAuthenticationDialog.h"
#include "ui_HttpAuthenticationDialog.h"

// Qt toolkit headers.
#include <QPushButton>
#include <QUrl>

// Construct the class.
HttpAuthenticationDialog::HttpAuthenticationDialog(QWidget *parentWidgetPointer, const QUrl &requestUrl, QAuthenticator *authenticatorPointer) :
                                                   QDialog(parentWidgetPointer), authenticatorPointer(authenticatorPointer)
{
    // Set the window title.
    setWindowTitle(i18nc("The HTTP authentication dialog window title.", "HTTP Authentication"));

    // Set the window modality.
    setWindowModality(Qt::WindowModality::ApplicationModal);

    // Instantiate the HTTP authentication dialog UI.
    Ui::HttpAuthenticationDialog httpAuthenticationDialogUi;

    // Setup the UI.
    httpAuthenticationDialogUi.setupUi(this);

    // Get handles for the widgets.
    QLabel *realmLabelPointer = httpAuthenticationDialogUi.realmLabel;
    QLabel *hostLabelPointer = httpAuthenticationDialogUi.hostLabel;
    usernameLineEditPointer = httpAuthenticationDialogUi.usernameLineEdit;
    passwordLineEditPointer = httpAuthenticationDialogUi.passwordLineEdit;
    QDialogButtonBox *dialogButtonBoxPointer = httpAuthenticationDialogUi.dialogButtonBox;
    okButtonPointer = dialogButtonBoxPointer->button(QDialogButtonBox::Ok);

    // Display the labels.
    realmLabelPointer->setText(authenticatorPointer->realm());
    hostLabelPointer->setText(requestUrl.host());

    // Connect the buttons.
    connect(dialogButtonBoxPointer, SIGNAL(accepted()), this, SLOT(authenticate()));
    connect(dialogButtonBoxPointer, SIGNAL(rejected()), this, SLOT(reject()));

    // Update the UI when the line edits change.
    connect(usernameLineEditPointer, SIGNAL(textEdited(const QString&)), this, SLOT(updateUi()));
    connect(passwordLineEditPointer, SIGNAL(passwordChanged(const QString&)), this, SLOT(updateUi()));

    // Initially disable the OK button.
    okButtonPointer->setEnabled(false);
}

void HttpAuthenticationDialog::authenticate()
{
    // Populate the authenticator.
    authenticatorPointer->setUser(usernameLineEditPointer->text());
    authenticatorPointer->setPassword(passwordLineEditPointer->password());

    // Close the dialog.
    close();
}

void HttpAuthenticationDialog::updateUi()
{
    // Update the OK button status
    if (usernameLineEditPointer->text().isEmpty() || passwordLineEditPointer->password().isEmpty())  // At least one of the line edits is empty.
    {
        // Disable the OK button.
        okButtonPointer->setEnabled(false);
    }
    else  // Both of the line edits are populated.
    {
        // Enable the OK button.
        okButtonPointer->setEnabled(true);
    }
}
