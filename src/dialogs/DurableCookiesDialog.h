/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022, 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DURABLECOOKIESDIALOG_H
#define DURABLECOOKIESDIALOG_H

// Qt toolkit headers.
#include <QDialog>
#include <QItemSelectionModel>
#include <QNetworkCookie>
#include <QSqlTableModel>

class DurableCookiesDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default constructor.
    explicit DurableCookiesDialog(QWidget *parentWidgetPointer);

Q_SIGNALS:
    // The signals.
    void addingCookie(const QNetworkCookie &cookie, const bool &addToDatabase) const;
    void deletingCookie(const QNetworkCookie &cookie) const;
    void updateParentUi() const;

private Q_SLOTS:
    // The private slots.
    void addCookie() const;
    void apply() const;
    void beforeDelete(int row) const;
    void beforeUpdate(int row, QSqlRecord &sqlRecord) const;
    void deleteAllCookies() const;
    void deleteCookie() const;
    void newCookie(QSqlRecord &sqlRecord) const;
    void ok();
    void reset() const;
    void updateUi() const;

private:
    // The private variables.
    QPushButton *applyButtonPointer;
    QPushButton *deleteAllCookiesButtonPointer;
    QPushButton *deleteCookieButtonPointer;
    QSqlTableModel *durableCookiesTableModelPointer;
    QPushButton *resetButtonPointer;
    QItemSelectionModel *tableSelectionModelPointer;
};
#endif
