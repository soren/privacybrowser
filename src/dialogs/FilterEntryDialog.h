/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILTER_ENTRY_DIALOG_H
#define FILTER_ENTRY_DIALOG_H

// Qt toolkit headers.
#include <QCheckBox>
#include <QDialog>
#include <QTableWidget>

class FilterEntryDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit FilterEntryDialog(QWidget *parentWidgetPointer, QTableWidget *tableWidgetPointer, const int initialRow, const QString &filterListTitle, const QString &sublistTitle, bool isAllowList);

private Q_SLOTS:
    // The private slots.
    void next();
    void previous();

private:
    // The private variables.
    QLineEdit *appliedEntryListLineEditPointer;
    QLineEdit *appliedFilterOptionsLineEditPointer;
    int currentRow;
    QLineEdit *domainLineEditPointer;
    QLineEdit *domainListLineEditPointer;
    QLineEdit *finalMatchLineEditPointer;
    QLineEdit *fontLineEditPointer;
    QCheckBox *hasRequestOptionsCheckBoxPointer;
    QLineEdit *imageLineEditPointer;
    QLineEdit *initialMatchLineEditPointer;
    bool isAllowList;
    QLineEdit *mainFrameLineEditPointer;
    QLineEdit *mediaLineEditPointer;
    QPalette negativeBackgroundPalette;
    QPushButton *nextButtonPointer;
    QPalette normalBackgroundPalette;
    QLineEdit *objectLineEditPointer;
    QLineEdit *originalEntryLineEditPointer;
    QLineEdit *originalFilterOptionsLineEditPointer;
    QLineEdit *otherLineEditPointer;
    QLineEdit *pingLineEditPointer;
    QPalette positiveBackgroundPalette;
    QPushButton *previousButtonPointer;
    QLineEdit *scriptLineEditPointer;
    QLineEdit *styleSheetLineEditPointer;
    QLineEdit *subFrameLineEditPointer;
    QTableWidget *tableWidgetPointer;
    QLineEdit *thirdPartyLineEditPointer;
    QLineEdit *xmlHttpRequestLineEditPointer;

    // The private functions.
    void populateDialog(const int row);
    void setFilterOptionBackgroundPalette(QLineEdit *lineEditPointer);
    void setInitialAndFinalMatchBackgroundPalette(QLineEdit *lineEditPointer);
};
#endif
