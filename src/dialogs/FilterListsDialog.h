/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILTER_LISTS_DIALOG_H
#define FILTER_LISTS_DIALOG_H

// Application headers.
#include "structs/FilterListStruct.h"

// Qt toolkit headers.
#include <QComboBox>
#include <QDialog>
#include <QTableWidget>
#include <QTextEdit>

class FilterListsDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit FilterListsDialog(QWidget *parentWidgetPointer);

private Q_SLOTS:
    void populateFilterListTextEdit(int filterListComboBoxIndex) const;
    void populateTableWidget(int sublistComboBoxIndex) const;
    void showFilterEntryDialog(int row);
    void sortTable(int column, Qt::SortOrder sortOrder) const;

private:
    // The private variables.
    QComboBox *filterListComboBoxPointer;
    QTextEdit *filterListTextEditPointer;
    QComboBox *sublistComboBoxPointer;
    QTableWidget *sublistTableWidgetPointer;

    // The private functions.
    std::forward_list<EntryStruct *>* getFilterListForwardList(FilterListStruct *filterListStructPointer) const;
    FilterListStruct* getFilterListStruct(int filterListComboBoxIndex) const;
    void populateFilterListComboBox(const FilterListStruct *filterListStructPointer) const;
};
#endif
