/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADDBOOKMARKDIALOG_H
#define ADDBOOKMARKDIALOG_H

// Application headers.
#include "helpers/FolderHelper.h"

// Qt toolkit headers.
#include <QDialog>
#include <QLineEdit>
#include <QRadioButton>
#include <QTreeWidget>

class AddBookmarkDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit AddBookmarkDialog(QWidget *parentWidgetPointer, const QString &bookmarkName, const QString &bookmarkUrl, const QIcon &favoriteIcon, const double parentFolderId);

Q_SIGNALS:
    // The signals.
    void bookmarkAdded() const;

private Q_SLOTS:
    // The private slots.
    void addBookmark();
    void browse();
    void updateUi();

private:
    // The private variables.
    FolderHelper *folderHelperPointer;

    // The private widgets.
    QPushButton *addButtonPointer;
    QLineEdit *bookmarkNameLineEditPointer;
    QLineEdit *bookmarkUrlLineEditPointer;
    QRadioButton *customFavoriteIconRadioButtonPointer;
    QTreeWidget *parentFolderTreeWidgetPointer;
    QRadioButton *websiteFavoriteIconRadioButtonPointer;
};
#endif
