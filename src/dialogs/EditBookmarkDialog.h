/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EDITBOOKMARKDIALOG_H
#define EDITBOOKMARKDIALOG_H

// Application headers.
#include "helpers/FolderHelper.h"
#include "structs/BookmarkStruct.h"

// Qt toolkit headers.
#include <QDialog>
#include <QLineEdit>
#include <QRadioButton>

class EditBookmarkDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit EditBookmarkDialog(QWidget *parentWidgetPointer, const int databaseId, QIcon &currentWebsiteFavoriteIcon);

Q_SIGNALS:
    // The signals.
    void bookmarkSaved() const;

private Q_SLOTS:
    // The private slots.
    void browse();
    void save();
    void updateUi();

private:
    // The private variables.
    FolderHelper *folderHelperPointer;

    // The private widgets.
    int bookmarkDatabaseId;
    QLineEdit *bookmarkNameLineEditPointer;
    BookmarkStruct *bookmarkStructPointer;
    QLineEdit *bookmarkUrlLineEditPointer;
    QRadioButton *currentFavoriteIconRadioButtonPointer;
    QRadioButton *currentWebsiteFavoriteIconRadioButtonPointer;
    QRadioButton *customFavoriteIconRadioButtonPointer;
    QTreeWidget *parentFolderTreeWidgetPointer;
    QPushButton *saveButtonPointer;
};
#endif
